package vectorise

import (
	"fmt"
)

type Vectorise struct {
	Input string
	Token []string
}

func New(input string) *Vectorise {
	v := &Vectorise{Input: input}
	fmt.Println(v)
	return v
}
