package vectorise

import "testing"

func TestVectoriser(t *testing.T) {
	input := "Buvez de ce whisky que le patron juge fameux."
	expected := []string{
		"Buvez",
		"de",
		"ce",
		"whisky",
		"que",
		"le",
		"patron",
		"juge",
		"fameux",
	}
	v := New(input)

	if len(v.Token) != len(expected) {
		t.Fatalf("expected %q, got %q", expected, v.Token)
	}

	for i := range expected {
		if expected[i] != v.Token[i] {
			t.Fatalf("expected %q, got %q", expected[i], v.Token[i])
		}
	}
}
